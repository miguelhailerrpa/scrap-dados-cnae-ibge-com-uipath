import re
import pandas as pd

index = []
data = []

# Dataframe (tabela) lido a partir do arquivo 'ModeloPlanilhaPython.xlsx' presente na aba 'Planilha Original'
df = pd.read_excel('PlanilhaFinal.xlsx', sheet_name='Planilha1')

# Lista com os acentos que se deseja identificar e remover
acento = re.compile(r'(Á|É|Í|Ó|Ú|Ã|ã|Ç|ç|/|:|;|\.|,|>|<|_|-|!|@|#|\$|%|\*|&|(|)|\+|=)')

# Matriz dicionário para a troca em cada caso onde: 'Á':'a' -> Chave:Valor
reps = {
	'Á':'A', 'À':'A', 'Ã':'A', 'Â':'A', 'Ä':'A', 'á':'a', 'à':'a', 'ã':'a', 'â':'a', 'ä':'a',
	'É':'E', 'È':'E', 'Ê':'E', 'Ë':'E', 'é':'e', 'è':'e', 'ê':'e', 'ë':'e',
	'Í':'I', 'Ì':'I', 'Ï':'I', 'î':'i', 'í':'i', 'ì':'i', 'î':'i', 'ï':'i',
	'Ó':'O', 'Ò':'O', 'Ô':'O', 'Õ':'O', 'Ö':'o', 'ó':'o', 'ò':'o', 'ô':'o', 'õ':'o', 'ö':'o',
	'Ú':'U', 'Ù':'U', 'Ü':'U', 'û':'u', 'ú':'u', 'ù':'u', 'û':'u', 'ü':'u',
	'Ç':'C', 'ç':'c',
	'.':'', ',':'', '!':'', '@':'', '#':'', '%':'', '¨':'', '&':'', '*':'', '(':'', ')':'', '-':'', 
	'/':'', '?':'', '|':'', ':':'', ';':'', '_':'', '+':'', '§':'', '<':'', '>':'',
	'0':'0','1':'1','2':'2','3':'3','4':'4','5':'5','6':'6','7':'7','8':'8','9':'9'
}

# Função que efetua a troca do caracter identificado
def replace_all(text, dic, num):
	for i, j in dic.items():
		for k in range(num):
			text = text.replace(i, j)
	return text

# Percorre o dataframe separando os títulos no 'index' e os dados em 'data'
for i, j in df.iteritems():
	index.append(i)
	data.append(j)

# Percorre o 'index' (Títulos) removendo as letras maiúsculas
for i, j in enumerate(index):
    index[i] = j.replace('A','a').replace('B','b').replace('C','c').replace('D','d').\
    			 replace('E','e').replace('F','f').replace('G','g').replace('H','h').\
    			 replace('I','i').replace('J','j').replace('K','k').replace('L','l').\
    			 replace('M','m').replace('N','n').replace('O','o').replace('P','p').\
    			 replace('Q','q').replace('R','r').replace('S','s').replace('T','t').\
    			 replace('U','u').replace('V','v').replace('W','w').replace('X','x').\
    			 replace('Y','y').replace('Z','z')

# O novo dataframe é gerado
df = pd.DataFrame(data).transpose().head(1000000000)
df.columns = index		# Define os títulos como o 'index' com apenas letras minúsculas

# Percorre o novo dataframe substituindo os caracteres identificados de acordo com a matriz
for i in range(len(df)):
	for j in range(len(df.iloc[i])):
		df.iloc[i][j] = replace_all(str(df.iloc[i][j]), reps, len(df))

# Escreve o dataframe em uma planilha XLSX com mesmo nome 'sobrescrevendo-a'
with pd.ExcelWriter('PlanilhaFinal.xlsx') as writer:  
    df.to_excel(writer, sheet_name='Planilha1', index = None, header=True)

    # Percorre todas as colunas definindo a largura de cada uma de acordo com seu texto
    for column in df:
        column_length = max(df[column].astype(str).map(len).max(), len(column))
        col_idx = df.columns.get_loc(column)
        writer.sheets['Planilha1'].set_column(col_idx, col_idx, column_length+2)

