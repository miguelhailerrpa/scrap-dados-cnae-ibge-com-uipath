Automação utilizando o UiPath para Realizar o Scrap de Dados do site - https://cnae.ibge.gov.br/

Esta automação tem como objetivo pegar todas as classes  dos CNAES das seções A,B e C.

O objetivo desta automação é meramente para estudos, para mostrar como realizar um scrap utilizando o Uipath. Ao final da automação será chamado um arquivo em Python para
realizar algumas formatações da Planilha final.


---

## Requistos para rodar a Automação

1. UiPath Studio Community - 2021.4.4.
2. Excel.

---

## Configurando

A automação está configurada para pegar as seções A,B e C do seguinte endereço: https://cnae.ibge.gov.br/?view=estrutura.

Caso desejar que a automação realize o scrap de outras seções, basta alterar o elemento Extract Structured na Seção Extração de Dados Seção. 
Nas propriedades do elemento(MaxNumberOfResults) Ajustar para o seu propósito, neste exercício está configurado para para pegar até 3(A,B e C).

---

## Resultado Final

Ao final da automação será criado na pasta do projeto uma planilha com o nome PlanilhaFinal.xlsx

Caso não tenha python instalado em sua máquina não há problemas , pois no final o sistema irá chamar um executável para formatar a Planilha.

No diretório da Automação está o arquivo de Python caso precise realizar ajustes em sua formatação.

